WPJ428 + SIMCOM 8202G
=====================

<div style="margin:auto">
<img src="img/boitier-v2.jpg" style="max-width: 600px"/>
</div>


**We see this project as a way to assess how well we can regain control/understanding on our NR5G/LTE routers.**  
*And for now the answer seems to be "very little"*.


## ⛔✋ THIS IS NOT AN HOW-TO GUIDE. 

During the process, it appears we made major mistakes. Because we had
already bought the components, it was too late and had to live with them or find workarounds. But for a new build,
it is advised to choose better components. Here are our major errors, that can be summarized as "the cost of trying to be on the edge":
  - The M.2 port of the board is only wired for the USB protocol 2.0 (and not USB 3.0 or PCI Express). The link is very slow and is probably 
our bottleneck which limits the connection to 10Mbps (!). Instead of choosing the new connector technology (M.2), we should have choosen
an older connector (mini PCI) that is available on many more tested and validated boards.
  - More generally, the WPJ428 board is not really amazing: it is targeted at "IoT" which is really a diverted way to say that is really "underpowered" in many aspects. Due to its niche target market, it was not well supported by OpenWRT, especially just after it's release. And of course, we bought it "just after its release".
  - The firmware of our LTE/NR5G modem is very buggy. Avoid obscure references and, instead, choose already widespread references.
Also, the market does not move at the same pace: while many smartphone are sold with support from NR5G, 
pluggable modems were still nascent, here again we paid the "early adopter" tax, with the UDP bug for example.
  - The support for LTE/NR5G is not optimal in OpenWRT. ModemManager is a great quality of life enhancer, but it is meant to work with NetworkManager, which is not integrated with OpenWRT. We had to write multiple flawed scripts to handle re-connections and the various quirks associated to mobile connections. Ideally, we should have written a daemon in a proper programming language (C or Lua) that observe/interact with ModemManager/NetworkManager through their dbus interface for a more reliable experience.
  - We did not invest enough time/energy on the antenna part. Having a clear line of sight and putting it above (and not below) the roof would have clearly improved performances. Also, your connection quality will heavily depend on your mobile provider's cell tower, and not all cell tower are born equals. Some of them are connected with fiber directly to the backbone, some of them are "meshed" through wireless connection with other ones. Some of them are also very overloaded, while other ones have only you as the client. Etc. Collecting information beforce deploying your router is very crucial.

In the end, we bought very expensive components to get only very mediocre performances (only 10Mbps while LTE alone supports up to 100Mbps).
Buying a NR5G modem was clearly a waste of money. 


## Our research & experiment process

Our case study is a NR5G router as the main Internet access point in a place where we have no signal on our cellphones.
Thus, we need external antennas that are put in the roof.
Additionnaly, we want to maximizme the throughput (both RX and TX) and minimize latency while keeping a stable connection over time.

Our project is currently made of the following physical parts:
  - A custom made case that is 3D printed - [Download files](./3D_model)
  - A Compex WPJ428 board - [wi-cat.ru](https://wikidevi.wi-cat.ru/Compex_WPJ428)
  - A Simcom 8202G modema - [official site](https://www.simcom.com/product/SIM8202GM2.html)
    - Connector: M.2 key B -> PCI Express x2, S-ATA, USB 2.0/3.0 (with HSIC/SSIC), I2C or Audio
  - Some antennas, the setup is not definitive yet
    - Log-Periodic Dipole Array (LPDA) Antenna - Directional antenna LTE 800/1800/2600 MHz, MIMO, SMA - [wimo](https://www.wimo.com/en/antennas/gsm-lte-mobile-phone-antennas/60072) - [diff between yagi and lpda](https://powerfulsignal.com/blog/whats-the-difference-between-an-lpda-antenna-and-a-yagi-antenna/)
    - A cheap cross antenna - [amazon](https://www.amazon.fr/gp/product/B00BBKKULW/ref=ppx_yo_dt_b_asin_title_o04_s00)
    - Some small wifi-like antenna - [wimo, similar but different](https://www.wimo.com/en/antennas/gsm-lte-mobile-phone-antennas/60022-sma)

We try to document our journey here:
  - Guides
    - [Our own OpenWRT image for the WPJ428](./doc/compile.md)
    - [Flash OpenWRT on the WPJ428](./doc/flash.md)
    - [Manually connect a modem to the network](./doc/manual-modem.md)
    - [Our OpenWRT configuration](./doc/config.md)
    - Developped scripts
  - Problems we encountered
    - ✅ Dimensions for the board in the datasheet are wrong
    - ✅ Ethernet ports are not configured at boot
    - ✅ [The UDP+Don't Fragment bug](./doc/udp-df.md)
    - ⌛ [Tracking the bandwidth bottleneck](./doc/bottleneck.md)
    - ⌛ Signal is not optimal with an antenna under the roof
    - ⌛ Our modem never connects on NR5G Non StandAlone (NSA) aka E-utran New radio Dual Connectivity (EN-DC)
  - Experience Reports
    - [Measurements next to the eNodeB](./doc/next_antenna.md)
  - Misc
    - [Our prototype: the TP-Link TL-WDR 3600 + Huawei e3372h](./doc/proto.md)
    - [Some logs/data/info about our hardware](./doc/logs.md)
    - [Bibliography](./doc/bib.md)
    - [Some mostly useless hacks](./doc/hacks.md)




