Compile:

```
clang -O2 -Wall -target bpf -c xdp_udp.c -o xdp_udp.o
```

Load:

```
ip link set dev lo xdp obj xdp_udp.o sec xdp_udp
```

View:

```
ip link show dev lo
```

Unload:

```
ip link set dev lo xdp off
```

Sources:
  - https://github.com/xdp-project/xdp-tutorial
  - https://github.com/xdp-project/xdp-tutorial/tree/master/basic01-xdp-pass
