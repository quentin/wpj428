## Logs

uboot logs:

```
Format: Log Type - Time(microsec) - Message - Optional Info
Log Type: B - Since Boot(Power On Reset),  D - Delta,  S - Statistic
S - QC_IMAGE_VERSION_STRING=BOOT.BF.3.1.1-00118
S - IMAGE_VARIANT_STRING=DAABANAZA
S - OEM_IMAGE_VERSION_STRING=CRM
S - Boot Config, 0x00000021
S - Reset status Config, 0x00000000
S - Core 0 Frequency, 0 MHz
B -       261 - PBL, Start
B -      1339 - bootable_media_detect_entry, Start
B -      1679 - bootable_media_detect_success, Start
B -      1693 - elf_loader_entry, Start
B -      5073 - auth_hash_seg_entry, Start
B -      7219 - auth_hash_seg_exit, Start
B -    577690 - elf_segs_hash_verify_entry, Start
B -    695375 - PBL, End
B -    695399 - SBL1, Start
B -    786439 - pm_device_init, Start
D -         7 - pm_device_init, Delta
B -    787885 - boot_flash_init, Start
D -     52818 - boot_flash_init, Delta
B -    844845 - boot_config_data_table_init, Start
D -      3835 - boot_config_data_table_init, Delta - (419 Bytes)
B -    852054 - clock_init, Start
D -      7575 - clock_init, Delta
B -    864102 - CDT version:2,Platform ID:8,Major ID:1,Minor ID:0,Subtype:0
B -    867516 - sbl1_ddr_set_params, Start
B -    872614 - cpr_init, Start
D -         2 - cpr_init, Delta
B -    876996 - Pre_DDR_clock_init, Start
D -         4 - Pre_DDR_clock_init, Delta
D -     13176 - sbl1_ddr_set_params, Delta
B -    890735 - pm_driver_init, Start
D -         2 - pm_driver_init, Delta
B -    960961 - sbl1_wait_for_ddr_training, Start
D -        28 - sbl1_wait_for_ddr_training, Delta
B -    976579 - Image Load, Start
D -    151978 - QSEE Image Loaded, Delta - (297240 Bytes)
B -   1128985 - Image Load, Start
D -      1447 - SEC Image Loaded, Delta - (2048 Bytes)
B -   1139420 - Image Load, Start
D -    211388 - APPSBL Image Loaded, Delta - (432219 Bytes)
B -   1351204 - QSEE Execution, Start
D -        60 - QSEE Execution, Delta
B -   1357421 - SBL1, End
D -    664130 - SBL1, Delta
S - Flash Throughput, 2007 KB/s  (731926 Bytes,  364613 us)
S - DDR Frequency, 537 MHz


U-Boot 2012.07 [Chaos Calmer 15.05.1,r35193] (Jul 25 2017 - 11:36:26)

smem ram ptable found: ver: 1 len: 3
DRAM:  256 MiB
machid : 0x8010000
NAND:  SF: Detected MX25L25635E with page size 4 KiB, total 32 MiB
ipq_spi: page_size: 0x100, sector_size: 0x1000, size: 0x2000000
32 MiB
MMC:   
*** Warning - bad CRC, using default environment

In:    serial
Out:   serial
Err:   serial
machid: 8010000
flash_type: 0
Hit any key to stop autoboot:  0 
Net:   MAC0 addr:4:f0:21:65:18:12
PHY ID1: 0x4d
PHY ID2: 0xd0b1
ipq40xx_ess_sw_init done
eth0
```

