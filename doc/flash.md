## Flash OpenWRT from uboot

(Configure your computer with a static IP address 192.168.1.2/24 and make sure you have a running tftp server containing your image)

First connect your UART cable on the router:

![UART](../img/uart.jpg)

Launch minicom

```
sudo minicom -D /dev/ttyUSB0
```

Then boot the router. Press repeatidly `[ENTER]` to get the following prompt:

```
(IPQ40xx) #
```

Then enter the following commands:

```
set ipaddr 192.168.1.1
set serverip 192.168.1.2
ping ${serverip}
sf probe
tftpboot 0x84000000 openwrt-ipq40xx-generic-compex_wpj428-squashfs-sysupgrade.bin
sf erase 0x00180000 +$filesize
sf write 0x84000000 0x00180000 $filesize
bootipq
```

## Flash OpenWRT from OpenWRT (!)

Copy the firmware in router's `/tmp`.
Why `/tmp`? Your router has more volatile storage than persistent one, moreover you will overwrite the persistent storage.
So, copy the firmware in `/tmp`!  
If you have a ssh connection, you can use `scp ./path/source/ userDest@ipDest:/tmp`   
Next, we will use the `sysupgrade` command. `-v` is verbose, `-n` ask openwrt to erase configuration files (ie. ask for a factory reset).

```
sysupgrade -v -n /tmp/openwrt-ipq40xx-generic-compex_wpj428-squashfs-sysupgrade.bin
```

## Flash OpenWRT from LuCi
With your binary image, you can flash from LuCi interface.  
*System -> Backup/Flash Firmware*  
You can **Uncheck** the **"Keep Settings"**, if you suppose than the problem come from a corruption of settings or you want erase the settings.

## Flash OpenWRT from Compex ROM (MimoAP)

*Not written*

## Flash MimoAP/OpenWRT with Compex tools on Uboot

*Not written*


