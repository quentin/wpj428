# Measurements next to the eNodeB

As we have poor performances, we took our router next to its eNodeB.
We built an adaptor to power the router from the car.

![In the car](../img/test-antenne-1.jpg)
![On top of the car](../img/test-antenne-2.jpg)

## Building the adaptor

*Not yet written*

## Experiment

Due to the situation, we wrote a quick diagnostic script named [wwan\_diag](../files/usr/sbin/wwan_diag).
Our raw results can be found here:

  - [2021-04-17_yagi_uda_and_cross_antenna_at_home.txt](../reports/2021-04-17_yagi_uda_and_cross_antenna_at_home.txt)
  - [2021-04-17_cross_antenna_near_goven_eNB.txt](../reports/2021-04-17_cross_antenna_near_goven_eNB.txt)
  - [2021-04-17_4_wifi_antenna_near_goven_eNB.txt](../reports/2021-04-17_4_wifi_antenna_near_goven_eNB.txt)
  - [2021-04-17_4_wifi_antenna_near_bruz_eNB.txt](../reports/2021-04-17_4_wifi_antenna_near_bruz_eNB.txt)

## Analysis

*Not yet written*
