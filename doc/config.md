## Final Modem Configuration

### Generic

  1. Set a password
  2. Set a name: `parangon`
  3. Copy/paste scripts in the `files/` folder if they are not shipped with the ROM
  4. Set IP range: `192.168.0.254/24`

### ModemManager config automatic
The image of this project relies on [UCI](https://openwrt.org/docs/guide-developer/uci-defaults) to configure ModemManager from the installation.  
In *files/etc/uci-defaults/10-modemmanager*:  
```
uci -q batch <<EOF
network.wan=interface
network.wan.device='/sys/devices/platform/soc/60f8800.usb2/6000000.dwc3/xhci-hcd.1.auto/usb3/3-1'
network.wan.proto='modemmanager'
network.wan.apn='free'
network.wan.iptype='ipv4v6'
network.wan.auth='none'
network.wan.peerdns='0'
network.wan.dns='9.9.9.10' '149.112.112.10' '2620:fe::10' '2620:fe::fe:10'

commit network
EOF
```

The interface **wan** (previously broadband) changed name because of the firewall. With the name **wan** the interface is added automatically in the firewall *wan*.  
Like we can see on:
```
root@OpenWrt:~# uci show firewall
...
firewall.@zone[1].name='wan'
firewall.@zone[1].input='REJECT'
firewall.@zone[1].output='ACCEPT'
firewall.@zone[1].forward='REJECT'
firewall.@zone[1].masq='1'
firewall.@zone[1].mtu_fix='1'
firewall.@zone[1].network='wan' 'wan6'
...
```

### ModemManager config deprecated

We append the following to ModemManager:

```
config interface 'broadband'
        option device '/sys/devices/platform/soc/60f8800.usb2/6000000.dwc3/xhci-hcd.1.auto/usb3/3-1'
        option proto 'modemmanager'
        option apn 'free'
        option iptype 'ipv4v6'
        option auth 'none'
        option peerdns '0'
        list dns '9.9.9.10'
        list dns '149.112.112.10'
        list dns '2620:fe::10'
        list dns '2620:fe::fe:10'
```

Then we can start the interface with:

```
ifup broadband
```

Add our broadband interface to the WAN zone in the firewall.

Both IPv6 and IPv4 should work:

```
ping openwrt.org
ping -6 openwrt.org
```


### Configure Wireguard

!!!!! BROKEN !!!!!!

On the router:

```bash
mkdir /tmp/x
cd /tmp/x
umask go=
wg genkey | tee wgclient.key | wg pubkey > wgclient.pub
wg genpsk > wgclient.psk
```

Add to `/etc/config/network`

```
config interface 'wg0'
	option proto 'wireguard'
	list addresses '192.168.2.3'
	option private_key '???'

config wireguard_wg0
	option public_key '???'
	option description 'rayonx'
	option persistent_keepalive '25'
	option endpoint_port '51820'
	list allowed_ips '192.168.2.0/24'
	option route_allowed_ips '1'
	option endpoint_host 'rayonx.machine.dufour.io'
```

Doc:

 - https://openwrt.org/docs/guide-user/services/vpn/wireguard/basics
 - https://openwrt.org/docs/guide-user/services/vpn/wireguard/client

### Configure SQM

*Not relevant now*


