## Using ModemManager

Wait for modems:

```
mmcli -M
```

Connect/Disconnect:

```
mmcli -m 0 --simple-connect="apn=free"
mmcli -m 0 --simple-disconnect
```

Set supported/preferred mode:

```
mmcli -m 0 --set-allowed-modes="3G|4G" --set-preferred-mode=4G
mmcli -m 0 --set-allowed-modes="5G"
```

Equivalent with `qmicli`:

```
qmicli --device=/dev/cdc-wdm0 --device-open-proxy --nas-get-system-selection-preference
qmicli --device=/dev/cdc-wdm0 --device-open-proxy --nas-set-system-selection-preference='5gnr|lte'
```

## QMICLI Connect

About our provider (Free Mobile), IPv4 is acquired through DHCP but IPv6 is statically provided.

### IPv6

```
qmicli --device=/dev/cdc-wdm0 --device-open-proxy --wds-start-network="ip-type=6,apn=free" --client-no-release-cid
qmicli --device=/dev/cdc-wdm0 --device-open-proxy   --wds-get-current-settings
ip addr add 2a0d:e487:15f:f7f5:5c58:4e37:90c0:74d8/64 dev wwan0
ip route add default via 2a0d:e487:15f:f7f5:49ec:2e4a:ca1:5b20
echo -e "nameserver 2a0d:e480::1\nnameserver 2a0d:e480::2" > /etc/resolv.conf
```

### IPv4

```
qmicli --device=/dev/cdc-wdm0 --device-open-proxy --wds-start-network="ip-type=4,apn=free" --client-no-release-cid
udhcpc -q -f -n -i wwan0
```
## More commands

For more debug commands please get a look at [wwan\_diag](../files/usr/sbin/wwan_diag).
