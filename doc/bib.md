## Sources

  - [All the commits in OpenWRT about the WPJ428](https://git.openwrt.org/?p=openwrt%2Fopenwrt.git&a=search&h=HEAD&st=commit&s=WPJ428)
  - https://openwrt.org/docs/guide-developer/build-system/use-buildsystem
  - https://damow.net/5g-home-broadband/
  - https://techship.com/faq/how-to-step-by-step-set-up-a-data-connection-over-qmi-interface-using-qmicli-and-in-kernel-driver-qmi-wwan-in-linux/
  - [No 5GNR, many qmicli commands to help debug and compare!](https://forums.quectel.com/t/rm500q-gl-not-working/6594)
  - https://forum.sierrawireless.com/t/mc7354-got-qmi-error-callfailed-when-starting-network/9053
  - https://community.autopi.io/t/error-when-starting-qmi-connection/2338
  - https://lists.freedesktop.org/archives/libqmi-devel/2018-January/002707.html
