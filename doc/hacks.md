## Get root on MimoAP

  1. Download a backup from your router firmware
  2. Extract it, edit `/etc/shadow`, replace the hash of the `root` user with the one from the `admin` user
  3. Recompress the folder hierarchy
  4. Import this new archive as backup in MimoAP
  5. Run `ssh root@192.168.1.1` (replace with the IP address of your router) and use your `admin` password
  6. Enjoy! (But you should really install a vanilla OpenWRT instead)


