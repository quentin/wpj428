## Fresh Compile

```bash
# Reset everything
make distclean

# Choose a specific version and init
git checkout openwrt-22.03
git pull
./scripts/feeds update -a
./scripts/feeds install -a

# Insert our additional files
rm -rf files/
cp -r ../wpj428/files ./

# Choose the packages we want/need
make menuconfig
```

Set the board:
 - Target System -> Qualcomm Atheros IPQ40XX
 - Target Profile -> Compex WPJ428 

Then activate:

 - Luci -> Collections -> Luci (default)
 - Luci -> Protocols -> luci-proto-modemmanager
 - Luci -> App -> luci-app-sqm
 - Luci -> App -> luci-app-openvpn
 - Luci -> Themes -> OpenWRT
 - Utilities -> qmi-utils (required by our script `promsignal`)
 - Utilities -> strace
 - Utilities -> Terminal -> Picocom

Check that these packages were activated automatically:
 - Kernel Modules -> USB -> kmod-usb-net-cdc-mbim
 - Kernel Modules -> USB -> kmod-usb-net-cdc-ncm
 - Network -> ModemManager
 - Network -> tcpdump
 - Utilities -> dbus

*Note: do not use `defconfig` / diffconfig / etc. because they do not work when we upgrade 
between major versions. They tend to create frankenstein builds. So really avoid them,
we lost enough time with this feature.*

```
make download
make -j $(nproc)
```

The output is in `bin/targets/ipq40xx/generic/`:

  - To restore this firmware from OpenWRT `openwrt-ipq40xx-generic-compex_wpj428-squashfs-sysupgrade.bin`
  - To restore this firmware from MimoAP (the OEM firmware) `openwrt-ipq40xx-generic-compex_wpj428-squashfs-cpximg-6a04.bin`

To restore it from OpenWRT:

```
scp firmware_image.bin root@192.168.1.1:/tmp
sysupgrade -n -v /tmp/firmware_image.bin
```
